from submodel import buildModel
import numpy as np
import matplotlib.pyplot as plt
import pyomo.environ as pyo
from pyomo.opt import SolverFactory, SolverStatus, TerminationCondition
import pandas as pd
import sys
from helpers import *
import math


class Problem:
    def __init__(self, S,  T, pricedata, inflowModel, params, opt, times, q_ymean):
        '''Initializing the Problem class. Creating all containers for values.
        Building all subproblems and guessing an optimal production plan,
        without considering future income.'''
        #Creating structure for a little overview of dimensions
        self.opt = opt                                                          # The chosen solver
        self.params = params                                                    # Parameters
        self.T = T                                                              # Planning horizon
        self.S = S                                                              # Scenarios to simulate
        self.I = len(params)                                                    # Number of reservoirs
        self.IM = inflowModel                                                   # The inflow model
        D2container = np.full(shape=(S,T), fill_value = -1.0)
        D3container = np.full(shape=(S,T,self.I), fill_value = -1.0)
        self.V = np.copy(D3container)                                           # Reservoir volumes
        self.Q = np.copy(D3container)                                           # Production
        self.B = np.copy(D3container)                                           # Bypass
        self.O = np.copy(D3container)                                           # Overflow
        self.Z = np.copy(D3container)                                           # Transformed inflow
        self.Y = np.copy(D2container)                                           # Sold power
        self.gamma = np.copy(D3container)                                       # Penalties
        self.EFI = np.copy(D2container)                                         # Expected future income
        self.subs = np.copy(D2container).tolist()                               # List of subproblems
        self.revenues = np.copy(D2container)                                    # Revenues in each stage
        self.objs = np.copy(D2container)                                        # Objective function values in each stage
        self.Vcoeffs = []                                                       # Water values for t = 1
        self.betas = []                                                         # Cut RHS for t = 1
        self.std = []                                                           # Standard deviations of each simulation
        self.dist = []                                                          # Total revenues of each simulation
        self.Upper = []                                                         # Upper bounds
        self.Lower = []                                                         # Lower bounds

        t0, t1, t2, t3, t4 = times
        self.times = times                                                      # Activation times for environmental constraints
        self.q_ymean = q_ymean                                                  # Inflow value to activate (i)

        #Assigning values
        self.p = pricedata                                                      # Price of power in each stage
        self.K = self.IM.e_t_k[0].shape[0] #All times must have the same K      # Number of possible inflow error realizations
        self.V0 = np.empty(self.I)                                              # Initial reservoir volumes
        for i in range(self.I):
            self.V0[i] = params[i]['V0']

        # In case we want to fix some random scenarios
        # self.picks = np.full(shape=(S,T), fill_value = 0)
        # for s in range(S):
        #     for t in range(T):
        #         self.picks[s,t] = np.random.choice(3)

        IM = self.IM
        objective = 0
        for s in range(S):
            for t in range(T):
                #Determining state of subproblem (s,t)
                if t == 0:
                    V_prev = self.V0
                    Z_prev = IM.Z0
                    realized_noise = [0,0,0,0] #Assuming no error in first week
                    Z = IM.cmat @ Z_prev + realized_noise
                else:
                    V_prev = self.V[s,t-1]
                    Z_prev = self.Z[s,t-1]
                    # realized_noise = self.noise[self.picks[s,t]] #In case we want to fix the scenarios
                    ind = np.random.choice(self.K, p = IM.p_dist)
                    realized_noise = IM.e_t_k[t%52,ind]
                    Z = IM.cmat @ Z_prev + realized_noise

                #Building subproblem
                model = buildModel(params, IM, t, times, q_ymean)
                d = setParams(V_prev, Z_prev, realized_noise, self.p[t], t)
                sub = model.create_instance(d)

                #Dummy variable for investigating infeasible constraints
                # for i in range(self.I):
                #     sub.dummy[i].fix()

                #Check if volume is conserved
                if np.equal(V_prev, vecVal(sub.V0)).all() == False:
                    print('V conservation error from ', t, ' to ', t+1)

                # Add end volume bounds
                if t == T-1:
                    # sub.gammaCost = 1e4
                    sub.cuts.add(sub.EFI <= 0)
                    sub.gammaCost = 1

                    endV = [params[i]['endV'] for i in range(self.I)]
                    def endVmin_rule(model, i):
                        return model.gamma[i] >= (endV[i][0] - model.V[i])
                    def endVmax_rule(model, i):
                        return model.gamma[i] >= (model.V[i] - endV[i][1])

                    sub.endVmin = pyo.Constraint(sub.I, rule = endVmin_rule)
                    sub.endVmax = pyo.Constraint(sub.I, rule = endVmax_rule)



                #Solving and saving results
                sub.EFI.fix()
                result = opt.solve(sub, warmstart = True)
                sub.EFI.unfix()
                sub.name = 'Time: '+str(t)+', Scen: '+str(s)
                checkFeasibility(sub, result, sub.name)
                self.subs[s][t] = sub
                self.EFI[s,t] = pyo.value(sub.EFI)
                self.revenues[s,t] = pyo.value(sub.p*sub.Y)
                self.objs[s,t] = pyo.value(sub.obj)
                objective += pyo.value(sub.obj)
                self.Z[s,t] = vecVal(sub.Z)
                self.V[s,t] = vecVal(sub.V)
                self.Q[s,t] = vecVal(sub.Q)
                self.B[s,t] = vecVal(sub.B)
                self.O[s,t] = vecVal(sub.O)
                self.gamma[s,t] = vecVal(sub.gamma)

                #Check if inflow is conserved, had some float comparison issues, hence the rounding
                if np.equal(Z.round(10), vecVal(sub.Z).round(10)).all() == False:
                    print('Inflow conservation error in', s,t)


        #Storing some trajectories to plot
        self.initialProduction = np.copy(self.Q)
        self.history = [np.copy(1/self.S*self.Q.sum(axis = (0,2)))]
        self.check = [np.copy(self.Z)]
        self.init_obj = 1/self.S * objective
        print('Objective of initiation: ',self.init_obj)


    def backPass(self):
        '''Creating cuts for stage T-1 to stage 0, by solving subproblems for
        stage T to stage 1. Only creating 8 cuts in each iteration, where
        subproblems to create cuts are chosen heuristically to create only the
        "most relevant" cuts.'''
        #Fetching some values for ease of access
        T = self.T
        S = self.S
        I = self.I
        opt = self.opt
        subs = self.subs
        K = self.K

        for t in [T-i-2 for i in range(T-1)]:
            #Using heuristic to choose subproblems to generate ''most relevant'' cuts
            sampleSampleS, rest = findScatteredVals(self.revenues[:,t+1], S)
            sampleSampleS.extend(np.random.choice(rest, size = 4, replace = False))

            for s in sampleSampleS:
                sub = subs[s][t+1] #Creating cut from t+1
                beta = 0
                pi_d = 0
                pi = np.zeros(shape = (I))
                pi2 = np.zeros(shape = (I))

                #Startvalues for t+1
                V_prev = self.V[s,t]
                Z_prev = self.Z[s,t]

                #Solving for all K realizations of inflow modelling error,
                # and creating cut coefficients.
                p_ind = 0
                p_lst = self.IM.p_dist
                for e in self.IM.e_t_k[(t+1)%52]:
                    p = p_lst[p_ind]
                    p_ind += 1
                    for i in range(I):
                        sub.e[i] = e[i]
                    opt.solve(sub, warmstart = True)
                    beta += p*pyo.value(sub.obj)

                    for i in range(I):
                        dual = sub.dual[sub.waterBalance[i]]
                        dual2 = sub.dual[sub.inflowTransition[i]]
                        pi[i] += p*dual
                        pi2[i] += p*dual2

                beta -= np.dot(V_prev,pi)
                beta -= np.dot(self.IM.f2((self.IM.cmat @ Z_prev), t+1), pi2)

                if t == 1:
                    self.Vcoeffs.append(pi)
                    self.betas.append(beta)

                #Adding expected cut all scenarios in stage t
                for u in range(S):
                    sub_prev = subs[u][t]
                    V_expr = pyo.summation(pi, sub_prev.V)
                    Z_expr = sum(self.IM.f(sum(self.IM.cmat[i,j] * sub_prev.Z[j] for j in sub_prev.I), t+1, i)*pi2[i] for i in sub_prev.I)
                    cut = sub_prev.EFI - V_expr - Z_expr <= beta
                    sub_prev.cuts.add(cut)


        #Calculating new upper limit
        lst = []
        for s in range(S):
            opt.solve(subs[s][0], warmstart = True)
            lst.append(pyo.value(subs[s][0].obj))

        self.Upper.append(max(lst)) #Should be equal anyway



    def forwardPass(self):
        '''Simulating new inflow scenarios, and solving all subproblems. As the
        backward recursion might alter the state of some subproblems, we assign
        them the state of the last simulation.'''
        #Fetching some values for ease of access
        S = self.S
        T = self.T
        I = self.I
        opt = self.opt
        subs = self.subs
        IM = self.IM
        t0, t1, t2, t3, t4 = self.times

        obj_lst = []
        for s in range(S):
            objective = 0
            for t in range(T):
                #Determining state of subproblem (s,t)
                if t == 0:
                    V_prev = self.V0
                    Z_prev = IM.Z0
                    realized_noise = [0,0,0,0] #Assuming no error in first week
                    Z = IM.cmat @ Z_prev + realized_noise
                else:
                    V_prev = self.V[s,t-1]
                    Z_prev = self.Z[s,t-1]
                    ind = np.random.choice(self.K, p = IM.p_dist)
                    # realized_noise = self.noise[self.picks[s,t]] #In case we want to fix the scenarios
                    realized_noise = IM.e_t_k[t%52,ind]
                    Z = IM.cmat @ Z_prev + realized_noise


                #Assigning the correct values to the subproblem
                sub = subs[s][t]
                for i in range(I):
                    sub.Z0[i] = Z_prev[i]
                    sub.e[i] = realized_noise[i]
                    sub.V0[i] = V_prev[i]


                #Solving and storing
                result = opt.solve(sub, warmstart = True)
                checkFeasibility(sub, result, sub.name)
                self.EFI[s,t] = pyo.value(sub.EFI)
                self.revenues[s,t] = pyo.value(sub.p*sub.Y)
                self.objs[s,t] = pyo.value(sub.obj)
                subObj = pyo.value(sub.obj) - pyo.value(sub.EFI)
                objective += subObj
                if t == T-1: objective += pyo.value(sub.EFI)
                self.Z[s,t] = vecVal(sub.Z)
                self.V[s,t] = vecVal(sub.V)
                self.Q[s,t] = vecVal(sub.Q)
                self.B[s,t] = vecVal(sub.B)
                self.O[s,t] = vecVal(sub.O)
                self.gamma[s,t] = vecVal(sub.gamma)

            obj_lst.append(objective)

        lower = 1/S * sum(obj_lst)
        this = 0
        for obj in obj_lst:
            this += (lower - obj)**2

        std = math.sqrt(1/(S**2) * this)
        #Updating lower bound
        self.std.append(std)
        self.Lower.append(lower)
        print('Standard deviation of Monte Carlo run: ', std)
        #And saving trajectories to plot
        self.history.append(np.copy(1/self.S*self.Q.sum(axis = (0,2))))
        self.check.append(np.copy(self.Z))
        self.dist.append(obj_lst)
