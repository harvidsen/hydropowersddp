from problemClass import Problem
import numpy as np
import matplotlib.pyplot as plt
import pyomo.environ as pyo
from pyomo.opt import SolverFactory
from inflowModel import inflowModel
from fileReaders import getData
from helpers import *
import seaborn as sns
import math
import time
import pandas as pd
# import cProfile # Module for profiling the algorithm

# # In case the working directory changes
# import os
# os.getcwd()
# os.chdir('C:\\Desired path\\..')

#t = 0 -> week 48, 2018
t0 = 20 #15. April. Starting point of uncertain activation of (i)
t1 = 24 #15. May. From here (i) is always in effect
t2 = 28 #fra 10.Juni. Fixed time for activation of (ii)
t3 = 37 #15. Aug. Start of (iii)
t4 = 39 #1. Sept. End of (iii)
times = [t0,t1,t2,t3,t4]



#Parameters
startWeek = 48 #Week number to start in
T = 52 #PLanning horizon, price data does not provide more than 3 years
S = 100 #Number of scenarios to simulate in the forward simulation
I = 4 #Number of reservoirs in the hydro system
maxIter = 30 #Max amount of iterations

priceFile = 'reg2.pri'
inflowSeries = ['586-E.txt', '598-E.txt', '1438-E.txt', '1529-E.txt']
parameterFile = 'BERGSDALEN.DETD'
startVolFile = 'uke1mag.vvber'
pricedata, inflowModel, params = getData(priceFile, inflowSeries,
                                        parameterFile, startVolFile,
                                        startWeek, T,
                                        endVolumes = 'endVolumes.txt')
pricedata = pricedata[32] #0 is the main series, 32 is the "worst case" series
q_ymean = 370/52 #Mean observed inflow (Mm3/year)/weeks for Hamlagrøvatn


#Choose a solver
import gurobipy
opt = SolverFactory('gurobi', solver_io = 'python')



#############################SMALL LOOP################################


times[2] = 23   #Value for t_c
s = time.time()

#Initializing
data = Problem(S,T, pricedata, inflowModel, params, opt, times, q_ymean)


# maxIter = 10   #In case we just want to test a few iterations
e = 1 #Just higher than tol
tol = 0
i = 1
log = open('theLog.txt', 'w')
penalties = []
timer = []
while e > tol or tol > 1:
    # print('Iteration: ', i)   #Keeping print statements for console logging
    print('Iteration', i)
    log.write('\n\nIteration: '+ str(i))
    s1 = time.time()
    log.close()
    data.backPass()
    print('Completed backpass')
    data.forwardPass()
    log = open('theLog.txt', 'a+')
    log.write('\nStandard deviation of MC '+str(data.std[-1]))
    penalties.append(data.gamma.sum())
    log.write('\nSum of penalties '+str(data.gamma.sum()))
    tol = 1.96*data.std[-1]
    e = abs(data.Upper[-1] - data.Lower[-1])
    log.write('\nError: '+str(e)+', tolerance: '+ str(tol))
    print('Error: '+str(e)+', tolerance: '+ str(tol))
    e1 = time.time()
    # print('done after', e1 - s1,'seconds')
    log.write('\ndone after '+str(e1-s1)+' seconds')
    timer.append(e1-s1)

    if i == maxIter:
        log.write('\nDid not converge after '+str(i)+ ' iterations')
        # print('Did not converge after', i, 'iterations')
        break
    i += 1

e = time.time()
log.write('\nConverged (?) after '+str(i)+' iterations, and '+str(e-s)+' seconds \n\n')
log.close()



# plotlyStuff(data, 'run', 'run')  # # Need update, as redundant values has been removed


hamlagroPlot(data)


plotPlan(data)


plotVolume(data)


plotEval(data)


d = {
    'Iteration': [j+1 for j in range(len(data.Upper))],
    'U' : data.Upper,
    'L' : data.Lower,
    'Confidence interval' : np.array(data.std)*1.96,
    'Penalties' : penalties,
    'Time' : timer
}


stats = pd.DataFrame(d)
stats.to_csv('stats_scenDep.csv')

data.Upper[-1]
data.Lower[-1]
data.std[-1]*1.96
data.gamma.sum()
data.O.sum()
res = open('res.txt', 'w')
res.write('U = {}\nL = {}\nstd = {}\ngamma = {}\nO = {}'.format(
    data.Upper[-1], data.Lower[-1], data.std[-1]*1.96, data.gamma.sum(), data.O.sum()))
res.close()




#Check validity of ec11
q = data.IM.f3(data.Z)
np.where(q[:,20,1] > q_ymean)[0].size
np.where(q[:,21,1] > q_ymean)[0].size
np.where(q[:,22,1] > q_ymean)[0].size
np.where(q[:,23,1] > q_ymean)[0].size
np.where(q[:,24,1] > q_ymean)[0].size
np.where(q[:,25,1] > q_ymean)[0].size
np.where(q[:,26,1] > q_ymean)[0].size
plt.plot(q[:,:,1].transpose())






#############################BIG LOOP################################
startTime = time.time()
bounds = []
revenues = []
penalties = []
confidence = []
trial_t_lst = [i for i in range(20, 38)]
# trial_t_lst = [29,30,33]
for t_val in trial_t_lst:
    times[2] = t_val
    data = Problem(S,T, pricedata, inflowModel, params, opt, times, q_ymean)
    e = 1 #Just higher than tol
    tol = 0
    i = 1
    logName = 'theLog_'+str(t_val)+'.txt'
    log = open(logName, 'w')
    s2 = time.time()
    while e > tol or tol > 1:
        log.write('\nIteration: '+ str(i))
        # print('Iteration: ', i)
        s1 = time.time()
        log.close()
        data.backPass()
        data.forwardPass()
        log = open(logName, 'a+')
        log.write('\nStandard deviation of MC '+str(data.std[-1]))
        log.write('\nSum of penalties '+str(data.gamma.sum()))
        tol = 1.96*data.std[-1]
        e = abs(data.Upper[-1] - data.Lower[-1])
        log.write('\nError: '+str(e)+', tolerance: '+ str(tol))
        i += 1
        e1 = time.time()
        # print('done after', e1 - s1,'seconds')
        log.write('\ndone after '+str(e1-s1)+' seconds')
        # print('done\n')

        if i == maxIter:
            log.write('\nDid not converge after '+str(i)+ ' iterations')
            # print('Did not converge after', i, 'iterations')
            break

    # print('Converged after', i, 'iterations, with t1 =', t_val,'\n\n')
    e2 = time.time()
    log.write('\nConverged (?) after '+str(i)+' iterations, and '+str(e2-s2)+' seconds \n\n')
    log.close()
    # plotlyStuff(data, 't='+str(t_val), 't='+str(t_val))
    bounds.append([data.Upper[-1], data.Lower[-1]])
    revenues.append(1/data.S * data.revenues.sum())
    penalties.append([data.gamma.sum(), data.O.sum()])
    confidence.append(1.96*data.std[-1])

    # # If plotting all schedules are necessary
    # hamlagroPlot(data)
    # plotPlan(data)
    # plotVolume(data)
    # plotEval(data)



endTime = time.time()
dt = endTime - startTime
f = open('totalTime.txt', 'w')
f.write('Everything finished after '+ str(round(dt, 3))+ 'seconds\nS='+str(data.S))
f.close()






#Plotting t1 analysis
t = [i for i in range(t0,t3+1)]
fig, ax = plt.subplots(nrows = 2, ncols = 1, figsize = (10,10))
fig.suptitle('t_c analysis')
ax[0].plot(t, [bounds[i][0] for i in range(len(bounds))], 'b')
ax[0].plot(t, [bounds[i][1] for i in range(len(bounds))], 'r')
ax[0].set_ylabel('M EUR')
ax[0].set_xlabel('t_c')
ax[0].grid(b = True)
ax[0].set_xticks(t)
L = np.array([bounds[i][1] for i in range(len(bounds))])
conf = np.array([confidence[i] for i in range(len(confidence))])
L_up = L + conf
L_down = L - conf
ax[0].plot(t, L_down, linestyle = '--', color = 'grey', alpha = 0.7)
ax[0].legend(['Upper bound', 'Estimated lower bound', '95% confidence of lower bound'])



ax2 = ax[1].twinx()
ax[1].plot(t, [penalties[i][0] for i in range(len(penalties))], 'b')
ax2.plot(t, [penalties[i][1] for i in range(len(penalties))], 'r')
ax[1].set_ylabel('gamma', color = 'b')
ax[1].set_xlabel('t_c')
ax[1].grid(b = True)
ax[1].set_xticks(t)
ax2.set_ylabel('overflow', color = 'r')
ax2.grid(b = False)
plt.savefig('t1_analysis.png')










#For pickling the pickle'able parts of the data instance
import pickle

for attribute in dir(data):
    if attribute not in ['IM', 'backPass', 'check', 'forwardPass', 'opt', 'subs'] and not attribute.startswith('__'):
        # print(attribute) # For determining where something f-ed up
        pickle.dump(eval('data.'+str(attribute)), open('.\\pickle\\'+ attribute + '.p', 'wb'))


i = 0
name = ['bounds', 'penalties', 'confidence']
for thing in [bounds, penalties, confidence]:
    pickle.dump(thing, open('.\\pickle\\'+ name[i] + '.p', 'wb'))
    i += 1



#####################################################################################
