import matplotlib.pyplot as plt
import numpy as np
import math
import pandas as pd
from statsmodels.tsa.vector_ar.var_model import VAR
from sklearn import decomposition


class inflowModel:
    '''Inflow model to be used'''
    def __init__(self, params, T, startT): #T must be length of observed scenarios in params[i]['inflowValues']
        # self.n_observed = 58
        I = 4
        year = 52
        self.K = 3
        self.I = I
        self.T = T
        self.p_dist = [0.2, 0.6, 0.2]

        serie_length = np.array(params[0]['inflowValues']).flatten().shape[0]
        q = np.zeros(shape = (serie_length, I))
        for i in range(I):
            q[:,i] = np.array(params[i]['inflowValues']).flatten()

        q = q[startT:] #To make t=0 as week 48, we skip the first 48 weeks (acceptable loss of data)
        self.data = q

        z = self.removeVariations(q)

        self.inputSignal = z

        mod = VAR(z) #Now t=0 -> week=startT
        res = mod.fit(maxlags = 1)
        cmat = res.coefs[0]
        self.cmat = cmat


        #PCA'ing for every week of the year
        self.e_t_k = np.zeros(shape = (year, self.K, self.I))
        self.e_t_size = np.zeros(shape = (year))
        self.e_t_ratio = np.zeros(shape = (year))
        self.e_t_unit = np.zeros(shape = (year, self.I))
        noise_scale = np.ones(shape = (year, self.I))

        #Scaling noise reservoir by reservoir, tedious and un-generalizable :(
        noise_scale[13:23, 0] = 0.8
        noise_scale[35:, 0] = 0.7
        noise_scale[[39,46], 0] = 0.4
        noise_scale[20, 0] = 0.2

        noise_scale[:20, 1] = 0.3
        noise_scale[20:23, 1] = 0.6
        noise_scale[23:34, 1] = 1
        noise_scale[[9,15], 1] = 0.2
        noise_scale[31:, 1] = 0.4

        noise_scale[:19, 2] = 0.4
        noise_scale[19:22, 2] = 0.6
        noise_scale[28:31, 2] = 0.8
        noise_scale[31:35, 2] = 0.6
        noise_scale[35:, 2] = 0.4
        noise_scale[43:46, 2] = 0.6
        noise_scale[9, 2] = 0.35

        noise_scale[:21, 3] = 0.4
        noise_scale[[21,22], 3] = 0.7
        noise_scale[29:34, 3] = 0.7
        noise_scale[30,3] = 1
        noise_scale[34:, 3] = 0.4
        noise_scale[40, 3] = 0.6
        noise_scale[41:, 3] = 0.5
        noise_scale[43:45, 3] = 0.7
        noise_scale[46, 3] = 0.4
        noise_scale[[9,12,15], 3] = 0.3

        self.noise_scale = noise_scale

        # noise_scale[:19] = 0.25 #Parameters to avoid negative inflow
        # noise_scale[19:30] = 0.6 #Maybe create a nice smooth function for scales?
        # noise_scale[30:40] = 0.4
        # noise_scale[40:] = 0.25
        for t in range(year):
            pca = decomposition.PCA(n_components=4)
            pca.fit(res.resid[t::year]) #Looking at the same weeks of all the years
            e = pca.components_[0] #Choosing only one principal component (+- for E[error] = 0)
            variance = pca.explained_variance_[0]
            zero = np.zeros(shape = self.I)
            e = np.multiply(e, noise_scale[t])
            self.e_t_k[t] = np.array([e, zero, -e])*variance #*noise_scale[t]
            self.e_t_unit[t] = e
            self.e_t_size[t] = variance
            self.e_t_ratio[t] = pca.explained_variance_ratio_[0]


        Z0 = z[0] #This is now the actual z
        self.Z0 = Z0

        # Simulating bounds
        Z_bounds = np.empty(shape = (3,T,I))
        for s in range(3):
            for t in range(T):
                if t == 0:
                    Z_prev = Z0
                else:
                    Z_prev = Z_bounds[s,t-1]

                if t == 0:
                    Z_bounds[s,t] = cmat@Z_prev
                else:
                    Z_bounds[s,t] = cmat@Z_prev + self.e_t_k[t%52,s]

        self.signal_bounds = Z_bounds
        #Transforming back
        Z_trans = np.empty(shape = (3, T, I))
        for s in range(3):
            for t in range(T):
                Z_trans[s,t] = self.f2(Z_bounds[s,t], t)

        #Warn if any inflow of lower bound came out negative
        for i in range(I):
            if Z_trans[2,:,i].min() < 0:
                neg_time = Z_trans[2,:,i].argmin()
                print('WARNIG!\nNegative inflow for reservoir', i, 'in time',neg_time)

        self.bounds = Z_trans



    #Remove season variatons
    def removeVariations(self, q):
        T = self.T
        I = self.I
        year = 52
        serie_length = q.shape[0]
        z = np.empty(shape = (serie_length,I))
        q_mean = np.empty(shape = (year,I))
        std = np.empty(shape = (year,I))
        for i in range(I):
            for t in range(year):
                observed_weeks = q[t::year,i] #Indexes same week number in all years
                n = observed_weeks.shape[0]
                mean = observed_weeks.mean()
                sum = 0
                for week in observed_weeks:
                    sum += (week - mean)**2

                std[t,i] = math.sqrt(sum/(n-1))
                q_mean[t,i] = mean
                z[t::year,i] = (q[t::year,i] - q_mean[t,i])/std[t,i]

        self.q_mean = q_mean
        self.std = std

        return z

    #Transform back one value, at time t and reservoir i
    def f(self, z, t, i):
        q = z*self.std[t%52,i] + self.q_mean[t%52,i]
        return q

    #Transform back one vector at time t
    def f2(self, vec, t):
        q = vec*self.std[t%52] + self.q_mean[t%52]
        return q

    #Transform all
    def f3(self, z):
        S, T, I = z.shape
        q = np.empty(shape = (S,T,I))

        std_temp = np.copy(self.std)
        q_mean_temp = np.copy(self.q_mean)

        reps = int(T/52)-1
        for rep in range(reps):
            std_temp = np.append(std_temp, std_temp[:52], axis = 0)
            q_mean_temp = np.append(q_mean_temp, q_mean_temp[:52], axis = 0)

        for s in range(S):
            q[s] = z[s]*std_temp + q_mean_temp

        return q


    def plotModel(self, save = False):
        #First, simulate some scenarios
        S = 50
        T = self.T
        Z_sim = np.zeros(shape = (S, T, self.I))
        for s in range(S):
            Z_prev = self.Z0
            for t in range(T):
                if t == 0:
                    ind = 1
                else:
                    ind = np.random.choice(self.K, p = self.p_dist)
                Z_sim[s,t] = self.cmat@Z_prev + self.e_t_k[t%52,ind]
                Z_prev = Z_sim[s,t]

        q_sim = self.f3(Z_sim)

        #Creating all subplots
        names = ['Torfinnsvatn', 'Hamlagrøvatn', 'Storafossen', 'Dale']
        plt.figure(figsize = (10,16))
        plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
        for i in range(self.I):
            plt.subplot(411+i)
            plt.title(names[i])
            if i == 3:
                plt.xlabel('t')
            plt.ylabel('Mm3')

            for s in range(S):
                plt.plot(q_sim[s,:,i], color = 'blue', alpha = 0.3)

            plt.plot(self.bounds[0,:,i], color = 'red')
            plt.plot(self.bounds[1,:,i], color = 'red')
            plt.plot(self.bounds[2,:,i], color = 'red')

        if save:
            plt.savefig('inflowSimulations.png')







################################################################################
