import numpy as np
########################Main methods########################

def readModules(f_name, f2_name):
    '''Funksjon laget for å lese filen BKK.DETD og hente relevant input for
    Prodrisk. Elementer ikke brukt fra .DETD filen er kommentert her. Elementer
    hentet ut er markert i Håkon sin notatversjon av ProdriskAPIspecs.'''
    f = open(f_name, 'r')
    f.readline() #Systeminfo
    line = f.readline().split(',') #Størrelsesinfo
    n_modules = int(line[0].strip())
    d_cycle = line[2].strip()

    #Lager modulliste, for å se når neste modul begynner
    mod_lst = []
    m = '0001'
    for mod in range(2, n_modules+1):
        id = '{:0>4}'.format(mod)
        mod_lst.append(id)
    mod_lst.append('end')

    data = {}
    mod = mod_lst[0]
    i = 1 #Viser til linje som forklart i Vtap-manual. IKKE som i .DETD filen.
    for line in f:
        e_lst = line.split(',')
        lst = [e.strip() for e in e_lst if e.strip()]

        if lst[0] == mod: #Sjekker om vi starter på ny modul
            m = mod
            mod_lst.pop(0)
            i = 1   #Lese ny modul
            mod = mod_lst[0]

        if i == 1: #Les som linje 1
            data = updateDict(data, 'number', m, int(lst[0])) #Intern nummerering
            data = updateDict(data, 'name', m, lst[1])
            #data['number'] = {m:int(lst[2])} #Ekstern nummerering
            data = updateDict(data, 'ownerShare', m, float(lst[3]))
            ###lst[3] = modultype(vann/vind)###

        elif i == 2: #Les som linje 2, osv...
            data = updateDict(data, 'rsvMax', m, float(lst[0]))
            ###lst[1] = reguleringsgrad
            data = updateDict(data, 'meanRegInflow', m, lst[2])
            ###lst[3] = navn på tilsigsserie for regulerbart tilsig
            ###lst[4,5] = start- og sluttår for middeltilsiget

        elif i == 3:
            data = updateDict(data, 'maxDischargeConst', m, float(lst[0]))
            data = updateDict(data, 'maxDischargeConst', m, float(lst[1]))
            data = updateDict(data, 'energyEquivalentConst', m, float(lst[2]))
            ###lst[3] = Bunnmagasin [Mm3], vanligvis 0 uansett

        elif i == 4:
            if int(lst[0]) == 0: #Bestemme om buffermagasin(0) eller ikke(1)
                data = updateDict(data, 'regulationType', m, 0)
            else:
                data = updateDict(data, 'regulationType', m, 1)
            np_refVol = int(lst[1]) #Antall punkt/koordinater for magasinstyrekurve(referansemagasin)

        elif i == 5: #Stykkevis beskrivelse av referansemagasin
            t = [int(e)*24*7 for e in lst[0::2] if e]
            y = [float(e) for e in lst[1::2] if e]
            data = updateDict(data, 'refVol', m, [t,y])

            while len(data['refVol'][m][0]) < np_refVol:
                e_lst = f.readline().split(',')
                lst = [e.strip() for e in e_lst if e.strip()]
                t = [int(e)*24*7 for e in lst[0::2]]
                y = [float(e) for e in lst[1::2]]
                data['refVol'][m][0].extend(t)
                data['refVol'][m][1].extend(y)
            ###x(uke),y(magasinreferanse) annenhver, som gitt over

        elif i == 6:
            data = updateDict(data, 'meanUnregInflow', m, float(lst[0]))
            ###lst[1] = Serienavn for uregulert tilsig

        elif i == 7:
            ###lst[1] = Midlere energiekvivalent, lik som i linje 3 her
            c = 0
            complete = False
            while c < 3: #Leser inn max 3 PQ kurver hvis det er spesifisert
                n_PQp = int(lst[0]) #antall punkt(XY) for PQ-kurver
                if n_PQp == -1 or n_PQp == 0:
                    break

                else:
                    e_lst = f.readline().split(',')
                    end_week = e_lst[0].strip()
                    lst = [float(e.strip()) for e in e_lst if e.strip()]
                    x = lst[1::2]
                    y = lst[2::2]
                    data = updateDict(data, 'PQcurve', m, [x,y])
                    if end_week == d_cycle:
                        complete = True

                    while len(data['PQcurve'][m][0]) < n_PQp:
                        e_lst = f.readline().split(',')
                        lst = [float(e.strip()) for e in e_lst if e.strip()]
                        x = lst[0::2]
                        y = lst[1::2]
                        data['PQcurve'][m][0].extend(x)
                        data['PQcurve'][m][1].extend(y)

                if complete == True:
                    break
                else:
                    e_lst = f.readline().split(',') #Skal være PQ spesifikasjon, ikke verdier
                    lst = [e.strip() for e in e_lst if e.strip()]
                    c += 1


        elif i == 8: #Tilsvarer da Linje nr. 13
            top_lst = [int(float(lst[0])), int(float(lst[1])), int(float(lst[2]))]
            data = updateDict(data, 'topology', m, top_lst)

        elif i == 9: #Ikke formatert rett her
            hyd_lst = [lst[0], lst[1], lst[2]]
            data = updateDict(data, 'hydraulicType', m, hyd_lst)

        elif i == 10: #Restriksjoner.
            while True:
                #Leser type først
                res_Type = int(lst[0])
                np = int(lst[1])
                flag = int(lst[2])
                if res_Type == 10:
                    break

                #Hopper til neste linje for å lese data tilknyttet type
                e_lst = f.readline().split(',')
                lst = [e.strip() for e in e_lst if e.strip()]
                if res_Type == 1:
                    t = [int(e) for e in lst[0::2]]
                    y = [float(e) for e in lst[1::2]]
                    data = updateDict(data, 'maxVol', m, [t,y])

                    while len(data['maxVol'][m][0]) < np:
                        e_lst = f.readline().split(',')
                        lst = [e.strip() for e in e_lst if e.strip()]
                        t = [int(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data['maxVol'][m][0].extend(t)
                        data['maxVol'][m][1].extend(y)

                elif res_Type == 2:
                    t = [int(e) for e in lst[0::2]]
                    y = [float(e) for e in lst[1::2]]
                    data = updateDict(data, 'minVol', m, [t,y])

                    while len(data['minVol'][m][0]) < np:
                        e_lst = f.readline().split(',')
                        lst = [e.strip() for e in e_lst if e.strip()]
                        t = [int(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data['minVol'][m][0].extend(t)
                        data['minVol'][m][1].extend(y)

                elif res_Type == 3:
                    if np == 1:
                        pass
                        ###lst[0] = Serienavn tilsigsserie
                        ###lst[0] = Midlere årlig vassføring
                    else:
                        t = [int(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data = updateDict(data, 'maxDischarge', m, [t,y])

                        while len(data['maxDischarge'][m][0]) < np:
                            e_lst = f.readline().split(',')
                            lst = [e.strip() for e in e_lst if e.strip()]
                            t = [int(e) for e in lst[0::2]]
                            y = [float(e) for e in lst[1::2]]
                            data['maxDischarge'][m][0].extend(t)
                            data['maxDischarge'][m][1].extend(y)

                elif res_Type == 4:
                    if np == 1:
                        pass
                        ###lst[0] = Serienavn tilsigsserie
                        ###lst[0] = Midlere årlig vassføring
                    else:
                        t = [int(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data = updateDict(data, 'minDischarge', m, [t,y])

                        while len(data['minDischarge'][m][0]) < np:
                            e_lst = f.readline().split(',')
                            lst = [e.strip() for e in e_lst if e.strip()]
                            t = [int(e) for e in lst[0::2]]
                            y = [float(e) for e in lst[1::2]]
                            data['minDischarge'][m][0].extend(t)
                            data['minDischarge'][m][1].extend(y)

                elif res_Type == 5:
                    if np == 1:
                        pass
                        ###lst[0] = Serienavn tilsigsserie
                        ###lst[0] = Midlere årlig vassføring
                    else:
                        t = [int(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data = updateDict(data, 'minBypass', m, [t,y])

                        while len(data['minBypass'][m][0]) < np:
                            e_lst = f.readline().split(',')
                            lst = [e.strip() for e in e_lst if e.strip()]
                            t = [int(e) for e in lst[0::2]]
                            y = [float(e) for e in lst[1::2]]
                            data['minBypass'][m][0].extend(t)
                            data['minBypass'][m][1].extend(y)

                elif res_Type == 6: #Angi buffermagasin
                    pass
                    ###lst[0] == 1 -> det er et buffermagasin

                elif res_Type == 7:
                    x = [float(e) for e in lst[0::2]]
                    y = [float(e) for e in lst[1::2]]
                    data = updateDict(data, 'volHeadCurve', m, [x,y])

                    while len(data['volHeadCurve'][m][0]) < np:
                        e_lst = f.readline().split(',')
                        lst = [e.strip() for e in e_lst if e.strip()]
                        x = [float(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data['volHeadCurve'][m][0].extend(x)
                        data['volHeadCurve'][m][1].extend(y)

                    e_lst = f.readline().split(',')
                    lst = [e.strip() for e in e_lst if e.strip()]
                    data = updateDict(data, 'submersion', m, float(lst[0]))
                    data = updateDict(data, 'nominalHead', m, float(lst[1]))

                elif res_Type == 8:
                    data = updateDict(data, 'plantName', m, lst[0][1:-1].strip())

                elif res_Type == 9:
                    x = [float(e) for e in lst[0::2]]
                    y = [float(e) for e in lst[1::2]]
                    data = updateDict(data, 'headDependentQMax', m, [x,y])

                    while len(data['headDependentQMax'][m][0]) < np:
                        e_lst = f.readline().split(',')
                        lst = [e.strip() for e in e_lst if e.strip()]
                        x = [float(e) for e in lst[0::2]]
                        y = [float(e) for e in lst[1::2]]
                        data['headDependentQMax'][m][0].extend(x)
                        data['headDependentQMax'][m][1].extend(y)
                    ###lst[:] = X(Fallhøyde), Y(Vannføring)

                e_lst = f.readline().split(',')
                lst = [e.strip() for e in e_lst if e.strip()]

        i += 1 #Gå til neste linje i fil
        if mod == 'end' and i == 11:
            break

    f.close()

    #Swapper keys og elementer. Datafil burde opprettes riktig til å begynne med.
    data = swapKeysElements(data)

    ###Les pumpedata###

    #Leser startfylling
    f = open(f2_name, 'r')
    for i in range(0,5):
        f.readline()

    modNames = {}
    for modNumber in data.keys():
        #modNames.append((modnumber,data[modNumber]['name']))
        modName = data[modNumber]['name'].replace('\'','').strip()
        modNames[modName] = modNumber


    for line in f:
        e_lst = line.split(',')
        lst = [e.replace('\'', '').strip() for e in e_lst if e.strip()]

        for name in modNames.keys():
            if lst[0] == name:
                data[modNames[name]].update({'startVol':float(lst[1])})
                #modNames.pop(name)

    return data


def readPriceSeries(priceFile, startT, T):
    f = open(priceFile, 'r')

    headLength = 8
    for line in range(0,headLength):
        f.readline()

    priceScenarios = []
    for line in f:
        e_lst = line.split(';')
        lst = [float(e.strip().replace(',','.')) for e in e_lst if e.strip()]
        priceScenarios.append(lst[int(startT)+1:int(startT+T)+1]) #Årskolonne retter 0-indekseringen

    f.close()

    return np.array(priceScenarios)


def readInflowSeries(series, relPath):
    data = {}

    for f_name in series:
        key = f_name[:-4]
        f = open(relPath+f_name, 'r')

        inflowWeeks = []
        for line in f:
            e_lst = line.split('\t')
            lst = [float(e.strip().replace(',','.')) for e in e_lst if e.strip()]
            inflowWeeks.append(lst)

        #Transponerer til ønsket format og samler
        tup = tuple(inflowWeeks)
        inflowYears = list(map(list, zip(*tup)))
        data[key] = inflowYears

    return data




########################Helpers########################

def swapKeysElements(d):
    dNew = {}
    for attName in d.keys():
        for objName in d[attName].keys():
            if objName in dNew.keys():
                dNew[objName].update({attName:d[attName][objName]})
            else:
                dNew[objName] = {attName:d[attName][objName]}

    return dNew

def updateDict(d, att, key, value): #For å lage og oppdatere keys
    if att not in d:
        d[att] = {}
    d[att][key] = value
    return d
