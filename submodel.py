from __future__ import division #Important when doing integer division
import pyomo.environ as pyo #Is timeconsuming to import
import numpy as np


'''
Decision variables are: V, Q, S, Y
'''

def buildModel(params, IM, t, times, q_ymean):
    #Extract parameters
    t0, t1, t2, t3, t4 = times
    I = len(params)
    pq = []
    topology = []
    for module in params.keys():
        pq.append(params[module]['pq'])
        topology.append(params[module]['top'])

    #Correct topology
    top = np.zeros(shape=(I,I), dtype = np.int8)
    for i in range(len(topology)):
        for j in range(I):
            if list(params.keys())[i]+1 == topology[j][0]:
                top[j,i] = 1


    #Build model
    model = pyo.AbstractModel()
    NonNegativeReals = pyo.NonNegativeReals
    model.I = pyo.RangeSet(0,I-1)

    #Variable bound rules
    def VBound_rule(model, i):
        return (0.001, params[i]['maxV'])


    def QBound_rule(model, i):
        return (0.0, params[i]['maxQ'])

    #Assigning variables
    model.V = pyo.Var(model.I, domain=NonNegativeReals,
                        bounds = VBound_rule, initialize=0)
    model.Q = pyo.Var(model.I, domain=NonNegativeReals,
                        bounds = QBound_rule, initialize=0)
    model.B = pyo.Var(model.I, domain=NonNegativeReals,
                        bounds = (0,1e3), initialize=0)
    model.O = pyo.Var(model.I, domain=NonNegativeReals,
                        bounds = (0,1e3), initialize=0)
    model.Y = pyo.Var(domain=NonNegativeReals, initialize=0)
    model.EFI = pyo.Var(domain=pyo.Reals, initialize=0,
                        bounds = (-5e5, 5e5)) #Should be greater than bounds imposed by cuts (sometimes negative)
    model.Z = pyo.Var(model.I, domain=pyo.Reals, initialize=0)
    model.gamma = pyo.Var(model.I, domain=NonNegativeReals, initialize=0,
                        bounds = (0, 5e3)) #Only needs to cover the magnitude og model.V
                                            #Negative values ruins soft constraints


    #Assigning changeable Parameters
    model.e = pyo.Param(model.I, domain=pyo.Reals, mutable = True, initialize = 0)
    model.Z0 = pyo.Param(model.I, domain=pyo.Reals, mutable = True, initialize = 0)
    model.V0 = pyo.Param(model.I, domain=NonNegativeReals, mutable = True, initialize = 0)
    model.p = pyo.Param(domain=NonNegativeReals, mutable = False)
    model.t = pyo.Param(model.I, domain=pyo.Integers, mutable = False)
    model.EC2V = pyo.Param(domain=NonNegativeReals, mutable = False, initialize = 141.31) #Value from xlsx: 141.31
    model.gammaCost = pyo.Param(domain=NonNegativeReals, mutable = True, initialize = 10) #100 gave zero penalty


    #Objective function
    def objective_function(model):
        return model.Y * model.p + model.EFI - model.gammaCost*pyo.summation(model.gamma)  #150000, var først (> pi)
        # - 1500*pyo.summation(model.O) for penalizing overflow

    model.obj = pyo.Objective(rule=objective_function, sense=pyo.maximize)

    #Water balance
    def waterBalance_rule(model, i):
        left = model.V[i]+model.Q[i]+model.B[i]+model.O[i]-sum(top[j,i]*(model.Q[j]+model.B[j]) for j in model.I)
        right = model.V0[i] + IM.f(model.Z[i], model.t[i], i) # + model.dummy[i]
        return left == right

    model.waterBalance = pyo.Constraint(model.I, rule = waterBalance_rule)


    #Inflow transition
    def inflowTransition_rule(model, i):
        return IM.f(model.Z[i], model.t[i], i) == IM.f(sum(IM.cmat[i,j] * model.Z0[j] for j in model.I) + model.e[i], model.t[i], i)

    model.inflowTransition = pyo.Constraint(model.I, rule = inflowTransition_rule)

    #Power balance
    def powerBalance_rule(model):
        return sum(pq[i]*model.Q[i] for i in model.I) - model.Y == 0


    model.powerBalance = pyo.Constraint(rule = powerBalance_rule)
    model.cuts = pyo.ConstraintList()
    model.dual = pyo.Suffix(direction = pyo.Suffix.IMPORT)


    # Environmental constraints
    def EC11_rule_Q(model):
        maxZ = IM.bounds[0,t,1]
        maxQ = params[1]['maxQ']
        a = -(maxQ)/(maxZ - q_ymean)
        b = (maxQ*maxZ)/(maxZ - q_ymean)
        return model.Q[1] - a*model.Z[1] - b <= 0

    def EC11_rule_B(model):
        maxZ = IM.bounds[0,t,1]
        maxB = 1e3
        a = -(maxB)/(maxZ - q_ymean)
        b = (maxB*maxZ)/(maxZ - q_ymean)
        return model.B[1] - a*model.Z[1] - b <= 0

    def EC12_rule(model):
        return model.Q[1] + model.B[1] == 0


    def softEC2_rule(model, i):
        if i == 1:
            expr = model.EC2V - model.V[1]
        else:
            expr = 0
        return model.gamma[i] >= expr

    def EC3_rule(model):
        return model.Q[1] + model.B[1] <= IM.f(model.Z[1], model.t[1], 1) + model.Q[0] + model.B[0]


    #Check for environmental constraints, max 3 years. #Should be generalized
    if t >= t0 and t < t1:
        model.EC11_1 = pyo.Constraint(rule = EC11_rule_Q)
        model.EC11_2 = pyo.Constraint(rule = EC11_rule_B)

    elif t >= t0+52 and t < t1+52:
        model.EC11_1 = pyo.Constraint(rule = EC11_rule_Q)
        model.EC11_2 = pyo.Constraint(rule = EC11_rule_B)

    elif t >= t0+104 and t < t1+104:
        model.EC11_1 = pyo.Constraint(rule = EC11_rule_Q)
        model.EC11_2 = pyo.Constraint(rule = EC11_rule_B)

    if t >= t1 and t < t2:
        model.EC12 = pyo.Constraint(rule = EC12_rule)

    elif t >= t1+52 and t < t2+52:
        model.EC12 = pyo.Constraint(rule = EC12_rule)

    elif t >= t1+104 and t < t2+104:
        model.EC12 = pyo.Constraint(rule = EC12_rule)

    if t >= t2 and t < t3:
        model.softEC2 = pyo.Constraint(model.I, rule = softEC2_rule)

    elif t >= t2+52 and t < t3+52:
        model.softEC2 = pyo.Constraint(model.I, rule = softEC2_rule)

    elif t >= t2+104 and t < t3+104:
        model.softEC2 = pyo.Constraint(model.I, rule = softEC2_rule)

    if t >= t3 and t <= t4:
        model.EC3 = pyo.Constraint(rule = EC3_rule)

    elif t >= t3+52 and t <= t4+52:
        model.EC3 = pyo.Constraint(rule = EC3_rule)

    elif t >= t3+104 and t <= t4+104:
        model.EC3 = pyo.Constraint(rule = EC3_rule)

    return model
