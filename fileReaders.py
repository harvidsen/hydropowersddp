from readData import readInflowSeries, readPriceSeries, readModules
import numpy as np
import pandas as pd
from inflowModel import inflowModel


####################%%Data reading and preprocessing%%##########################




def getData(priceFile, inflowSeries, parameterFile, startVolFile,
                                                startT, T, endVolumes = False):

    #Relative path to hide data from .git folder
    relPath = '..\\runData\\'

    ##########Reading datafiles#################################################
    priceData = readPriceSeries(relPath+priceFile, startT, T)

    inflowData = readInflowSeries(inflowSeries, relPath)

    modelParams = readModules(relPath + parameterFile,
                                relPath + startVolFile)

    #Pricedata read in EUR/Mwh
    priceData = priceData*1e3*1e-6 #10^6 EUR/Gwh

    ##########Pre-processing####################################################

    #Inflowkeys, connecting inflow series to correct module
    inflowKeys = {  '0001': '1529-E',
                    '0002': '598-E',
                    '0003': '1438-E',
                    '0004': '586-E'}

    #Extracting necessary model parameters and scaling to appropriate units
    params = {}
    for m in modelParams.keys():
        # pq = np.mean(modelParams[m]['PQcurve'][1])#*1e-2
        V0 = modelParams[m]['startVol']
        top = modelParams[m]['topology']
        maxQ = modelParams[m]['PQcurve'][1][-1] #m3/s
        maxV = modelParams[m]['rsvMax'] #Mm3
        pq = modelParams[m]['energyEquivalentConst'] #kwh/m3
        meanZ = float(modelParams[m]['meanRegInflow']) #Mm3/year
        inflowKey = inflowKeys[m]

        #Converting to similar units, Mm3 is million cubic meters, not megacubicmeters
        maxQ = maxQ *7*24*60*60 *1e-6 #Mm3/week
        pq = pq*1e-6*1e6 #Gwh/Mm3 (price is originally in EUR/Mwh, change whatever accordingly alternatively)
                            #Now it is in 10^6EUR/Gwh

        ind = int(m[2:])-1 #Changing to a convenient index
        params[ind] = {'pq': pq, 'top': top, 'maxV': maxV,
                        'maxQ': maxQ, 'inflowSeries': inflowKey,
                        'meanZ': meanZ, 'V0': V0}


    # Correcting inflow values for each module
    for key in params:
        module = params[key]
        inflowKey = module['inflowSeries']
        profiles = np.array(inflowData[inflowKey])
        meanZ = module['meanZ']

        #Hardcoding 1981-2015 as reference period for inflow, as all modules of Bergsdalen share this
        k = (30*meanZ)/np.sum(profiles[23:53])
        inflow = profiles*k
        params[key]['inflowValues'] = inflow


    #Adding end volumes, if specified
    if endVolumes:
        endV = np.loadtxt(relPath+endVolumes)
        for key in params:
            params[key]['endV'] = (endV[0,key], endV[1,key])


    #Creating and getting the inflow model
    inflow_model = inflowModel(params, T, startT)

    #Only returning one priceSerie
    return priceData, inflow_model, params


#
# #Extracting data
# def getData(priceFile, inflowSeries, parameterFile, startVolFile, T):
#     startTimeWeek = '201848' #Tilsigsfilen er på ukenummer, ikke måneder
#     nSimWeeks = '156' #3 år, bare fordi det stemmer overens med BKK.DETD
#     priceScenarios = readPriceSeries('..\\runData\\reg2.pri', startTimeWeek, nSimWeeks)
#     modelParams = readModules('..\\runData\\BERGSDALEN.DETD','..\\runData\\uke1mag.vvber')
#     series = ['586-E.txt', '598-E.txt',
#                 '1438-E.txt', '1529-E.txt']
#     inflowData = readInflowFiles(series) #Lagre listetransponeringen i eget kult bibliotek
#
#     series = ['586-E.txt', '598-E.txt',
#                 '1438-E.txt', '1529-E.txt']
#     inflowData = readInflowFiles(series) #Lagre listetransponeringen i eget kult bibliotek
#
#     # len(inflowData['598-E'][0])
#
#     #Fixing priceData, priceScenarios start in week 4 and are 156 weeks long
#     #Choosing 1958 scenario from week 52+4 to week 104+4
#     # priceData = np.empty([1, T]) #Allowing for easier extension to multiple priceScenarios
#     # priceData[0,:] = priceScenarios[0][52:52+T]
#     priceData = priceScenarios[:][:T]
#     # priceData[0,:] = priceScenarios[0][0:T] #For bruke 3 år, med den første
#
#
#     # Modules of Bergsdalen
#     modules = ['0001', '0002', '0003', '0004']
#     V0 = []
#     for mod in modules:
#         V0.append(modelParams[mod]['startVol'])
#
#     #Inflowkeys
#     inflowKeys = {  '0001': '1529-E',
#                     '0002': '598-E',
#                     '0003': '1438-E',
#                     '0004': '586-E'}
#
#     #Extracting necessary model parameters
#     params = {}
#     for m in modules:
#         # pq = np.mean(modelParams[m]['PQcurve'][1])#*1e-2
#         V0 = modelParams[m]['startVol']
#         top = modelParams[m]['topology']
#         maxQ = modelParams[m]['PQcurve'][1][-1] #m3/s
#         maxV = modelParams[m]['rsvMax'] #Mm3
#         pq = modelParams[m]['energyEquivalentConst'] #kwh/m3
#         meanZ = float(modelParams[m]['meanRegInflow']) #Mm3/year
#         inflowKey = inflowKeys[m]
#
#         #Converting to similar units, Mm3 is million cubic meters, not megacubicmeters
#         maxQ = maxQ *7*24*60*60 *1e-6 #Mm3/week
#         pq = pq *1e-3 *1e6 #Mwh/Mm3 (price is in EUR/Mwh, change whatever accordingly alternatively)
#
#         name = int(m[2:]) #Should be integers from 1->n_modules
#         params[name] = {'pq': pq, 'top': top, 'maxV': maxV,
#                         'maxQ': maxQ, 'inflowSeries': inflowKey,
#                         'meanZ': meanZ, 'V0': V0}
#
#
#     # Correcting inflow values for each module
#     for key in params:
#         module = params[key]
#         inflowKey = module['inflowSeries']
#         profiles = np.array(inflowData[inflowKey])
#         meanZ = module['meanZ']
#
#         #Hardcoding 1981-2015 as reference period for inflow, as all modules of Bergsdalen share this
#         k = (30*meanZ)/np.sum(profiles[23:53])
#         inflow = profiles*k
#         params[key]['inflowValues'] = inflow
#
#
#
#     #Shifting the key
#     goodParams = {}
#     for key in params:
#         goodParams[key-1] = {}
#         goodParams[key-1]['inflowValues'] = params[key]['inflowValues']
#         goodParams[key-1]['pq'] = params[key]['pq']
#         goodParams[key-1]['maxQ'] = params[key]['maxQ']
#         goodParams[key-1]['maxV'] = params[key]['maxV']
#         goodParams[key-1]['top'] = params[key]['top']
#         goodParams[key-1]['V0'] = params[key]['V0']
#
#
#     #Adding end volumes
#     endV = np.loadtxt('endVolumes.txt')
#     for key in goodParams:
#         goodParams[key]['endV'] = (endV[0,key], endV[1,key])
#
#     rainModel = inflowModel(goodParams, T)
#
#     return priceData, rainModel, goodParams
