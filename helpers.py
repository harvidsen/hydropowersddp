import numpy as np
import matplotlib.pyplot as plt
import pyomo.environ as pyo
from pyomo.opt import SolverFactory, SolverStatus, TerminationCondition
import pandas as pd
import sys
import plotly as pl
import plotly.graph_objs as go


def vecVal(pyoVector):
    '''Retrieve vector from pyo indexed component'''
    I = 4 #Get from variable
    a = np.empty(4)
    for i in range(I):
        a[i] = pyo.value(pyoVector[i])

    return a

def setParams(V_prev, Z_prev, e, p, t):
    d = {None: {
        'V0': {},
        'Z0': {},
        'e': {},
        'p': {None: p},
        't': {}
        }}

    for i in range(V_prev.size):
        d[None]['V0'].update({i : V_prev[i]})
        d[None]['Z0'].update({i : Z_prev[i]})
        d[None]['e'].update({i : e[i]})
        d[None]['t'].update({i : t})

    return d

def checkFeasibility(sub, result, name):
    if result.solver.termination_condition == TerminationCondition.optimal:
        pass
    else:
        condition = str(result.solver.termination_condition)
        sub.pprint()
        sys.exit('Instance, ' + name + ' is ' + condition)

    return


def findScatteredVals(a, S):
    '''Takes and array of size S, finds indexes of 4 scattered values.
    Returns the 4 indexes and the rest separately'''
    lst = []
    rest = []
    revs = np.copy(a)
    for s in range(S//2):
        if s == 0:
            max, min = revs.argmax(), revs.argmin()
            lst.extend([max,min])
            revs = np.delete(revs, [max, min])

        elif s == S//4:
            max, min = revs.argmax(), revs.argmin()
            lst.extend([max,min])
            revs = np.delete(revs, [max, min])

        else:
            max, min = revs.argmax(), revs.argmin()
            rest.extend([max,min])
            revs = np.delete(revs, [max, min])

    return lst, rest

def find2ScatteredVals(a, b, S):
    '''Takes two arrays of size S and finds the index 4 scattered values in
     each array. If any index is equal, then a random index (not from spread)
     is used.'''
    a_ind = np.argsort(a)
    b_ind = np.argsort(b)

    a_spread = a_ind[[0, S//3, 2*S//3, S-1]]
    b_spread = b_ind[[0, S//3, 2*S//3, S-1]]

    copy = np.copy(b_spread)
    for ind in b_spread:
     if ind in a_spread:
         replace = np.random.choice([i for i in range(S) if i not in a_spread and i not in b_spread])
         copy[np.where(copy == ind)[0]] = replace

    lst = np.concatenate((a_spread, copy))
    rest = np.array([i for i in range(S) if i not in lst])

    return lst.tolist(), rest.tolist()




def plotStuff(data):
    fig = plt.figure(figsize = (10,16))
    plt.subplot(321)
    plt.plot(data.IM.f3(data.Z)[0])
    plt.ylabel('Z')
    plt.xlabel('t')
    plt.legend(['1','2','3','4'])
    plt.title('Inflow of one scenario')

    plt.subplot(322)
    plt.plot(data.p)
    plt.ylabel('p')
    plt.xlabel('t')
    # plt.legend(['0','1','2','3'])
    plt.title('Price')

    plt.subplot(323)
    plt.plot(data.V[0])
    plt.ylabel('V')
    plt.xlabel('t')
    plt.legend(['1','2','3','4'])
    plt.title('Volumes, scen: 0')

    plt.subplot(324)
    plt.plot(data.Q[0])
    plt.ylabel('Q')
    plt.xlabel('t')
    plt.legend(['1','2','3','4'])
    plt.title('Production, scen: 0')

    n = len(data.history)
    op = np.linspace(0.4, 1, num = 2)
    op = np.append(op, 1)
    plt.subplot(325)
    plt.plot(data.history[0].transpose(), color = 'c')
    j = 0
    for i in range(n):
        if i > n-3:
            plt.plot(data.history[i].transpose(), color = 'k', alpha = op[j])
            j += 1

    plt.ylabel('Q')
    plt.xlabel('t')
    plt.title('History, total production')

    plt.subplot(326)
    it = len(data.Lower)
    x = [i for i in range(1,it+1)]
    line = np.array([data.Lower[-1] for i in range(it)])
    line2 = np.array([data.Upper[-1] for i in range(it)])
    gap = data.Upper[-1] - data.Lower[-1]
    rel = data.Lower[-1]/data.Upper[-1]
    text = 'Gap: '+str(gap)+' EUR\nRelative size: ' + str(rel)
    bottom = 1.01*data.Lower[0]
    plt.plot(x, data.Lower, '-*', x, data.Upper, '-*', x, line, 'r--', x, line2, 'r--')
    plt.text(3,bottom, text)
    plt.ylabel('EUR')
    plt.xlabel('Iteration')
    plt.title('Bounds')

    params = data.params
    cols = ['Torfinnsvatn(1)', 'Hamlagrøvatn(2)', 'Storafossen(3)', 'Dale(4)']
    rows = ['pq [kwh/m3]', 'maxQ [m3/s]', 'maxV [Mm3]', 'Runs to']
    table = np.full((4,4), fill_value=None)
    for key in params:
        maxQ = (params[key]['maxQ']*1e6)/(7*24*60*60)
        pq = params[key]['pq']*1e3 *1e-6
        table[0, key] = round(pq, 2)
        table[1,key] = round(maxQ,2)
        table[2, key] = params[key]['maxV']

        out = params[key]['top'][0]
        if out == 0:
            table[3, key] = 'Sealevel'
        else:
            table[3, key] = cols[out-1]


    df = pd.DataFrame(table, index = rows, columns = cols)
    plt.savefig('results.png')
    return df


def plotlyStuff(data, name1, name2):
    time = np.linspace(0,data.T-1,data.T, dtype = int)

    iScens = data.IM.f3(data.Z)
    # iMeans = data.IM.f3(data.Zmean)
    trace_lst = []
    main_colors = ['rgb(204,0,0)', 'rgb(0,204,0)', 'rgb(0,0,204)', 'rgb(204,0,204)']
    sub_colors = ['rgb(255,102,102)', 'rgb(102,255,102)', 'rgb(102,102,255)', 'rgb(255,102,255)']
    names = ['Torfinnsvatn', 'Hamlagrøvatn', 'Storafossen', 'Dale']

    #Inflow lines
    for i in range(data.I):
        c = main_colors[i]
        # trace_lst.append(go.Scatter(x = time, y = iMeans[0,:,i],
        #                             line = {'color':c, 'width':2},
        #                             name = names[i]))
        c = sub_colors[i]
        for s in range(data.S):
            trace_lst.append(go.Scatter(x = time, y = iScens[s,:,i],
                                        line = {'color':c},
                                        opacity = 0.4,
                                        hoverinfo = 'none',
                                        showlegend = False))

    #Price line
    price_trace = go.Scatter(x = time, y = data.p, name = 'Price', showlegend = False,
                            xaxis = 'x2', yaxis = 'y2')

    #Parameter table
    params = data.params
    cols = ['Torfinnsvatn<br>(1)', 'Hamlagrøvatn<br>(2)', 'Storafossen<br>(3)', 'Dale<br>(4)']
    rows = ['pq<br>[kwh/m3]', 'maxQ<br>[m3/s]', 'maxV<br>[Mm3]', 'Runs<br>to']
    table = np.full((4,4), fill_value=None)
    for key in params:
        maxQ = (params[key]['maxQ']*1e6)/(7*24*60*60)
        pq = params[key]['pq']*1e3 *1e-6
        table[0, key] = round(pq, 2)
        table[1,key] = round(maxQ,2)
        table[2, key] = params[key]['maxV']

        out = params[key]['top'][0]
        if out == 0:
            table[3, key] = 'Sealevel'
        else:
            table[3, key] = cols[out-1]
    df = pd.DataFrame(table, index = rows, columns = cols)
    rowname = list(df.index)
    colname = list(df.columns)
    table = go.Table(
                header = dict(values = [' '] + rowname,
                            fill = dict(color = 'rgb(192,192,192)')),
                cells = dict(values = [colname,df.iloc[0],df.iloc[1],df.iloc[2],df.iloc[3]],
                            fill = dict(color = ['rgb(192,192,192)', 'white', 'white', 'white', 'white'])),
                domain = dict(x = [0.68,1], y = [0,0.45])
    )

    #For general use, you can make a pl.tools.make_subplots first.
    #To construct a generic layout to alter
    layout = dict(xaxis1 = dict(domain=[0,0.64], anchor = 'y1', title = 't'),
                    yaxis1 = dict(domain=[0,1], anchor = 'x1', title = 'Mm3'),
                    xaxis2 = dict(domain=[0.71,1], anchor = 'y2', title = 't'),
                    yaxis2 = dict(domain=[0.625,1], anchor = 'x2', title = 'EUR/Mwh'),
                    xaxis3 = dict(domain=[0.71,1], anchor = 'y2'),
                    yaxis3 = dict(domain=[0,0.375], anchor = 'x2'),
                    legend = dict(orientation = 'h'),
                    title = 'Input',
                    annotations = [{'font': {'size': 16},
                                 'showarrow': False,
                                 'text': 'Recent simulated inflows',
                                 'x': 0.32222222222222224,
                                 'xanchor': 'center',
                                 'xref': 'paper',
                                 'y': 1.0,
                                 'yanchor': 'bottom',
                                 'yref': 'paper'},
                                {'font': {'size': 16},
                                 'showarrow': False,
                                 'text': 'Price',
                                 'x': 0.8555555555555556,
                                 'xanchor': 'center',
                                 'xref': 'paper',
                                 'y': 1.0,
                                 'yanchor': 'bottom',
                                 'yref': 'paper'},
                                {'font': {'size': 16},
                                 'showarrow': False,
                                 'text': 'Parameters',
                                 'x': 0.8555555555555556,
                                 'xanchor': 'center',
                                 'xref': 'paper',
                                 'y': 0.45,
                                 'yanchor': 'bottom',
                                 'yref': 'paper'}])

    #Environmental constraint line
    t0, t1, t2, t3, t4 = data.times
    q_ymean = data.q_ymean
    meanYrI1 = go.Scatter(x = time[t0:t1+1], y = [q_ymean for i in range(len(time[t0:t1]))],
                        line = dict(dash = 'dash', color = 'grey'),
                        showlegend = False)
    meanYrI2 = go.Scatter(x = time[t1:t2], y = [q_ymean for i in range(len(time[t1:t2]))],
                        line = dict(dash = 'dash', color = 'red'),
                        showlegend = False)
    trace_lst.append(meanYrI1)
    trace_lst.append(meanYrI2)

    if data.T > 52:
        meanYrI12 = go.Scatter(x = time[t0+52:t1+1+52], y = [q_ymean for i in range(len(time[t0:t1]))],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        meanYrI22 = go.Scatter(x = time[t1+52:t2+52], y = [q_ymean for i in range(len(time[t1:t2]))],
                            line = dict(dash = 'dash', color = 'red'),
                            showlegend = False)
        trace_lst.append(meanYrI12)
        trace_lst.append(meanYrI22)

    if data.T > 104:
        meanYrI12 = go.Scatter(x = time[t0+104:t1+1+104], y = [q_ymean for i in range(len(time[t0:t1]))],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        meanYrI22 = go.Scatter(x = time[t1+104:t2+104], y = [q_ymean for i in range(len(time[t1:t2]))],
                            line = dict(dash = 'dash', color = 'red'),
                            showlegend = False)
        trace_lst.append(meanYrI12)
        trace_lst.append(meanYrI22)


    fig = go.Figure(data = trace_lst + [price_trace, table], layout = layout)
    pl.offline.plot(fig, filename = '.\\outs\\in_' + name1 + '.html', auto_open = False)

    #And output
    V = data.V
    Q = data.Q
    B = data.B
    O = data.O

    #Reservoir lines
    Vmean = V.mean(axis = 0)
    Qmean = Q.mean(axis = 0)
    Bmean = B.mean(axis = 0)
    Omean = O.mean(axis = 0)
    Vmean.shape
    Qmean.shape
    main_colors = ['rgb(204,0,0)', 'rgb(0,204,0)', 'rgb(0,0,204)', 'rgb(204,0,204)']
    sub_colors = ['rgb(255,102,102)', 'rgb(102,255,102)', 'rgb(102,102,255)', 'rgb(255,102,255)']
    names = ['Torfinnsvatn', 'Hamlagrøvatn', 'Storafossen', 'Dale']
    reservoir_traces = []
    Q_traces = []
    B_traces = []
    O_traces = []
    for i in range(data.I):
        reservoir_traces.append([])
        Q_traces.append([])
        B_traces.append([])
        showLeg = True
        if i > 0:
            showLeg = False
        reservoir_traces[i].append(go.Scatter(x = time, y = Vmean[:,i],
                                    line = {'color':main_colors[2], 'width':2},
                                    name = 'Volume',
                                    showlegend = showLeg))
        Q_traces[i].append(go.Scatter(x = time, y = Qmean[:,i],
                                    line = {'color':main_colors[1], 'width':2},
                                    name = 'Production',
                                    showlegend = showLeg))
        B_traces[i].append(go.Scatter(x = time, y = Bmean[:,i],
                                    line = {'color':main_colors[0], 'width':2},
                                    name = 'Bypass',
                                    showlegend = showLeg))
        O_traces.append(go.Scatter(x = time, y = Omean[:,i],
                                    line = {'color':'rgb(204,0,204)', 'width':2},
                                    name = 'Overflow',
                                    showlegend = showLeg))
        for s in range(data.S):
            reservoir_traces[i].append(go.Scatter(x = time, y = V[s,:,i],
                                        line = {'color':sub_colors[2]},
                                        opacity = 0.4,
                                        hoverinfo = 'none',
                                        showlegend = False))
            Q_traces[i].append(go.Scatter(x = time, y = Q[s,:,i],
                                        line = {'color':sub_colors[1]},
                                        opacity = 0.4,
                                        hoverinfo = 'none',
                                        showlegend = False))
            B_traces[i].append(go.Scatter(x = time, y = B[s,:,i],
                                        line = {'color':sub_colors[0]},
                                        opacity = 0.4,
                                        hoverinfo = 'none',
                                        showlegend = False))
            # O_traces[i].append(go.Scatter(x = time, y = O[s,:,i],
            #                             line = {'color':'rgb(204,0,204)'},
            #                             opacity = 0.6,
            #                             hoverinfo = 'none',
            #                             showlegend = False))




    #Total production history lines
    #The if it will go into before storing
    prod_traces = []
    prod = data.history
    prod_traces.append(go.Scatter(x = time, y = prod[0],
                                line = {'color':'rgb(96,96,96)'},
                                showlegend = False,
                                hoverinfo = 'none'))
    if len(prod) >= 2:
        n = len(prod[1:])
        for i in range(n):
            showHov = 'none'
            style = 'dash'
            color = 'rgb(0,128,255)'
            if i == n-1:
                showHov = 'x+y'
                style = 'solid'
                color = 'rgb(0,0,255)'
            opacity = ((i+1)**4)/(n**4)
            if opacity < 0.001:
                pass
            else:
                prod_traces.append(go.Scatter(x = time, y = prod[i+1],
                                            line = {'color': color},
                                            opacity = opacity,
                                            showlegend = False,
                                            hoverinfo = showHov))


    U = data.Upper
    L = data.Lower
    std = data.std

    L_up = []
    for i in range(len(L)):
        L_up.append(L[i] + 1.96*std[i])

    L_do = []
    for i in range(len(L)):
        L_do.append(L[i] - 1.96*std[i])

    conv_traces = []
    x = [i+1 for i in range(len(U))]
    conv_traces.append(go.Scatter(x = x, y = U,
                                showlegend = False))
    conv_traces.append(go.Scatter(x = [0] + x, y = [data.init_obj] + L,
                                showlegend = False))
    conv_traces.append(go.Scatter(x = x, y = L_up,
                                hoverinfo = 'none',
                                showlegend = False,
                                line = dict(dash = 'dash',
                                            color = 'grey')))
    conv_traces.append(go.Scatter(x = x, y = L_do,
                                hoverinfo = 'none',
                                showlegend = False,
                                line = dict(dash = 'dash',
                                            color = 'grey')))
    x = []
    for i in range(len(data.dist)):
        for s in range(data.S):
            x.append(i+1)
    y = [item for sublist in data.dist for item in sublist]
    conv_traces.append(go.Scatter(x = x, y = y, mode = 'markers',
                                    hoverinfo = 'none',
                                    showlegend = False))

    fig = pl.tools.make_subplots(rows = 4, cols = 2,
                                specs = [[{},{'rowspan':2}],
                                        [{},None],
                                        [{},{'rowspan':2}],
                                        [{},None]],
                                subplot_titles = [names[0], 'History',
                                                names[1], names[2],
                                                'convergence', names[3]],
                                print_grid = False)

    #Environmental constraint lines
    #EC plotting can be generalized
    Vmin = pyo.value(data.subs[0][0].EC2V)
    maxY2 = data.params[1]['maxV']
    t0_trace = go.Scatter(x = [t0,t0], y = [0, maxY2],
                        line = dict(dash = 'dash', color = 'grey'),
                        showlegend = False)
    t1_trace = go.Scatter(x = [t1,t1], y = [0, maxY2],
                        line = dict(dash = 'dash', color = 'grey'),
                        showlegend = False)
    EC2_trace = go.Scatter(x = [t2,t2,t3,t3], y = [0, Vmin, Vmin, 0],
                        line = dict(dash = 'dash', color = 'red'),
                        showlegend = False)
    t3_trace = go.Scatter(x = [t4,t4], y = [0, maxY2],
                        line = dict(dash = 'dash', color = 'grey'),
                        showlegend = False)
    fig.append_trace(t0_trace, 2, 1)
    fig.append_trace(t1_trace, 2, 1)
    fig.append_trace(EC2_trace, 2, 1)
    fig.append_trace(t3_trace, 2, 1)

    if data.T > 52:
        t0_trace2 = go.Scatter(x = [t0+52,t0+52], y = [0, maxY2],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        t1_trace2 = go.Scatter(x = [t1+52,t1+52], y = [0, maxY2],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        EC2_trace2 = go.Scatter(x = [t2+52,t2+52,t3+52,t3+52], y = [0, Vmin, Vmin, 0],
                            line = dict(dash = 'dash', color = 'red'),
                            showlegend = False)
        t3_trace2 = go.Scatter(x = [t4+52,t4+52], y = [0, maxY2],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        fig.append_trace(t0_trace2, 2, 1)
        fig.append_trace(t1_trace2, 2, 1)
        fig.append_trace(EC2_trace2, 2, 1)
        fig.append_trace(t3_trace2, 2, 1)

    if data.T > 104:
        t0_trace2 = go.Scatter(x = [t0+104,t0+104], y = [0, maxY2],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        t1_trace2 = go.Scatter(x = [t1+104,t1+104], y = [0, maxY2],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        EC2_trace2 = go.Scatter(x = [t2+104,t2+104,t3+104,t3+104], y = [0, Vmin, Vmin, 0],
                            line = dict(dash = 'dash', color = 'red'),
                            showlegend = False)
        t3_trace2 = go.Scatter(x = [t4+104,t4+104], y = [0, maxY2],
                            line = dict(dash = 'dash', color = 'grey'),
                            showlegend = False)
        fig.append_trace(t0_trace2, 2, 1)
        fig.append_trace(t1_trace2, 2, 1)
        fig.append_trace(EC2_trace2, 2, 1)
        fig.append_trace(t3_trace2, 2, 1)

    #Adding lines
    for i in range(data.I):
        fig.append_trace(O_traces[i], i+1, 1)
        for s in range(data.S+1):
            fig.append_trace(reservoir_traces[i][s], i+1, 1)
            fig.append_trace(Q_traces[i][s], i+1, 1)
            fig.append_trace(B_traces[i][s], i+1, 1)


    for trace in prod_traces:
        fig.append_trace(trace, 1, 2)

    for trace in conv_traces:
        fig.append_trace(trace, 3, 2)



    fig.layout.update(legend = dict(orientation = 'h'),
                        title = 'Results')
                        # yaxis1 = dic(title = 'V [Mmv]1'),
                        # yaxis2 = dic(title = 'Total Production [Mwh]'),
                        # yaxis3 = dic(title = 'V [Mmv]2'),
                        # yaxis4 = dic(title = 'V [Mmv]3'),
                        # yaxis5 = dic(title = 'Expected income [EUR]'),
                        # yaxis6 = dic(title = 'V [Mmv]4'),
                        # xaxis2 = dic(title = 't'),
                        # xaxis5 = dic(title = 'Iteration'),
                        # xaxis6 = dic(title = 't2'))

    fig.layout['yaxis1'].update(title = 'Mm3')
    fig.layout['yaxis2'].update(title = 'Total Production [Mwh/week]')
    fig.layout['yaxis3'].update(title = 'Mm3')
    fig.layout['yaxis4'].update(title = 'Mm3')
    fig.layout['yaxis5'].update(title = 'Expected income [EUR]')
    fig.layout['yaxis6'].update(title = 'Mm3')
    fig.layout['xaxis2'].update(title = 't')
    fig.layout['xaxis5'].update(title = 'Iteration')
    fig.layout['xaxis6'].update(title = 't2')

    pl.offline.plot(fig, filename = '.\\outs\\res_' + name2 + '.html', auto_open = False)


def plotVolume(data, save = True):
    time = np.linspace(0,data.T-1,data.T, dtype = int)
    names = ['Torfinnsvatn', 'Hamlagrøvatn', 'Storafossen', 'Dale']

    #A figure containing 4 reservoir plots, each with multiple schedule traces
    fig = pl.tools.make_subplots(
        rows = 4,
        cols = 1,
        subplot_titles = names,
        print_grid = False
    )

    #Setting layout on the figure (and supressing return output)
    myLayout = fig.layout.update(
        title = 'Volume trajectories',
        width = 800,
        height = 1130,
        legend = dict(tracegroupgap = 196),
    )

    #Y axis labels
    for i in range(1, data.I+1):
        axis = 'yaxis'+str(i)
        myLayout[axis].update(title = 'Mm3')

    #X axis label
    myLayout['xaxis4'].update(title = 't')

    # #Creating traces for volume plots
    # Vquant = np.quantile(data.V, [0, 0.25, 0.5, 0.75, 1], axis = 0)

    # showLeg = True
    legGroup_lst = [1,2,3,4]
    for j in range(1, data.I+1): #Favoring plotlys 1-index
        serie = data.V[:,:,j-1]
        traces = detailPlot(serie, time, palette = 'blue', legGroup = legGroup_lst[j-1])

        for trace in traces:
            fig.append_trace(trace, j, 1)


    if save:
        name = str(data.times[2])
        pl.offline.plot(fig, filename = '.\\outs\\vols_t' + name + '.html', auto_open = False)
    else:
        pl.offline.plot(fig)


def plotPlan(data, save = True):
    #Inflow, pris og total produksjon
    names = ['Total simulated inflow', 'Price', 'Total Production']
    time = np.linspace(0,data.T-1,data.T, dtype = int)

    #A figure containing 4 reservoir plots, each with multiple schedule traces
    fig = pl.tools.make_subplots(
        rows = 3,
        cols = 1,
        subplot_titles = names,
        print_grid = False
    )

    #Setting layout on the figure (and supressing return output)
    myLayout = fig.layout.update(
        title = 'Production plan',
        width = 800,
        height = 1130,
        legend = dict(tracegroupgap = 320)
    )

    myLayout['yaxis1'].update(title = 'Mm3')
    myLayout['yaxis2'].update(title = 'EUR/Mwh')
    myLayout['yaxis3'].update(title = 'Mwh')
    myLayout['xaxis3'].update(title = 't')

    inflow = data.IM.f3(data.Z).sum(axis = 2)
    inflow_traces = detailPlot(inflow, time, palette = 'blue', legGroup = 'inflow')

    for trace in inflow_traces:
        fig.append_trace(trace, 1, 1)

    # Price trace
    Ptrace = go.Scatter(
        x = time,
        y = data.p*1e3,
        name = 'Price',
        legendgroup = 'price',
        line = dict(
            color = 'rgb(0,128,255)'
        )
    )
    fig.append_trace(Ptrace, 2, 1)


    # Production plot
    series = [data.initialProduction.sum(axis = 2), data.Q.sum(axis = 2)]
    names = ['Initial schedule', 'Last schedule']
    prod_traces = doublePlot(series, time, names, palettes = ['grey', 'blue'], legGroup = 'production')

    for trace in prod_traces:
        fig.append_trace(trace, 3, 1)

    if save:
        name = str(data.times[2])
        pl.offline.plot(fig, filename = '.\\outs\\plan_t' + name + '.html', auto_open = False)
    else:
        pl.offline.plot(fig)


def plotEval(data, save = True):
    #Plot evaluation
    names = ['Convergence', 'Spill', 'Penalty']
    time = np.linspace(0,data.T-1,data.T, dtype = int)

    #A figure containing 4 reservoir plots, each with multiple schedule traces
    fig = pl.tools.make_subplots(
        rows = 3,
        cols = 1,
        subplot_titles = names,
        print_grid = False
    )

    myLayout = fig.layout.update(
        title = 'Evaluation',
        width = 800,
        height = 1130,
        legend = dict(tracegroupgap = 320)
    )

    myLayout['yaxis1'].update(title = 'M EUR')
    myLayout['yaxis2'].update(title = 'Mm3')
    myLayout['yaxis3'].update(title = '')
    myLayout['xaxis1'].update(title = 'Iteration')
    myLayout['xaxis2'].update(title = 't')
    myLayout['xaxis3'].update(title = 't')

    #Convergence traces
    U = data.Upper
    L = data.Lower
    std = data.std

    L_up = []
    for i in range(len(L)):
        L_up.append(L[i] + 1.96*std[i])

    L_do = []
    for i in range(len(L)):
        L_do.append(L[i] - 1.96*std[i])


    x = [i+1 for i in range(len(U))]
    fig.append_trace(go.Scatter(x = x, y = U,
                                legendgroup = 'convergence',
                                name = 'Upper limit'), 1, 1)
    fig.append_trace(go.Scatter(x = [0] + x, y = [data.init_obj] + L,
                                legendgroup = 'convergence',
                                name = 'Estimated Lower limit'), 1, 1)
    fig.append_trace(go.Scatter(x = x, y = L_up,
                                hoverinfo = 'none',
                                legendgroup = 'convergence',
                                name = 'Confidence interval',
                                line = dict(dash = 'dash',
                                            color = 'grey')), 1, 1)
    fig.append_trace(go.Scatter(x = x, y = L_do,
                                hoverinfo = 'none',
                                showlegend = False,
                                line = dict(dash = 'dash',
                                            color = 'grey')), 1, 1)
    x = []
    for i in range(len(data.dist)):
        for s in range(data.S):
            x.append(i+1)
    y = [item for sublist in data.dist for item in sublist]
    fig.append_trace(go.Scatter(x = x, y = y, mode = 'markers',
                                    marker = dict(
                                        opacity = 0.5
                                    ),
                                    hoverinfo = 'none',
                                    legendgroup = 'convergence',
                                    name = 'Simulations'), 1, 1)

    #Spill traces
    first = True
    k = 1 #Numbering for traces and determining whats what
    for schedules in [data.O, data.B]: #First and last schedules
        prod = schedules.sum(axis = 2) #Total production
        quant = np.quantile(prod, [0, 0.25, 0.5, 0.75, 1], axis = 0)

        if first:
            colors = ['rgb(255, 102, 102)', 'rgb(255, 0, 0)', 'rgb(153, 0, 0)']
        else:
            colors = ['rgb(102, 178, 255)', 'rgb(0, 128, 255)', 'rgb(0, 76, 153)']


        #Extreme band
        fill_spec = 'none'
        for i in [0, -1]:
            trace = go.Scatter(
                x = time,
                y = quant[i,:],
                legendgroup = 'spill',
                showlegend = False,
                hoverinfo = 'none',
                name = str(k),
                fill = fill_spec,
                line = dict(
                    width = 0.1,
                    color = colors[0]
                )
            )
            k += 1
            fig.append_trace(trace, 2, 1)
            fill_spec = 'tonexty'

        #Quartile band
        fill_spec = 'none'
        for i in [1, -2]:
            trace = go.Scatter(
                x = time,
                y = quant[i,:],
                legendgroup = 'spill',
                showlegend = False,
                hoverinfo = 'none',
                name = str(k),
                fill = fill_spec,
                line = dict(
                    width = 0.1,
                    color = colors[1]
                )
            )
            k += 1
            fig.append_trace(trace, 2, 1)
            fill_spec = 'tonexty'

        #Mean line
        if first:
            name = 'Overflow'
        else:
            name = 'Bypass'
        trace = go.Scatter(
            x = time,
            y = quant[2,:],
            legendgroup = 'spill',
            showlegend = True,
            name = name,
            line = dict(
                color = colors[2],
                width = 2
            )
        )
        fig.append_trace(trace, 2, 1)
        first = False


    #Penalty traces
    serie = data.gamma.sum(axis = 2)
    quant = np.quantile(serie, [0, 0.25, 0.5, 0.75, 1], axis = 0)
    colors = ['rgb(255, 102, 102)', 'rgb(255, 0, 0)', 'rgb(153, 0, 0)']

    #Mean trace
    trace = go.Scatter(
        x = time,
        y = quant[2,:],
        name = 'Mean penalty',
        showlegend = True,
        hoverinfo = 'none',
        line = dict(
            dash = 'dash',
            color = colors[0]
        ),
        legendgroup = 'penalty'
    )
    fig.append_trace(trace, 3, 1)

    #min and max traces
    fill_spec = 'none'
    for j in [0,-1]:
        if j == 0:
            showLeg2 = True
        else:
            showLeg2 = False

        trace = go.Scatter(
            x = time,
            y = quant[j,:],
            name = 'Extreme values',
            showlegend = showLeg2,
            fill = fill_spec,
            line = dict(
                color = colors[1]
            ),
            legendgroup = 'penalty'
        )
        fig.append_trace(trace, 3, 1)
        fill_spec = 'tonexty'

    #Quartile band
    fill_spec = 'none'
    for j in [1,-2]:
        if j == 1:
            showLeg2 = True
        else:
            showLeg2 = False

        trace = go.Scatter(
            x = time,
            y = quant[j,:],
            name = 'Quartiles',
            showlegend = showLeg2,
            fill = fill_spec,
            hoverinfo = 'none',
            line = dict(
                dash = 'dot',
                color = colors[2]
            ),
            legendgroup = 'penalty'
        )
        fig.append_trace(trace, 3, 1)
        fill_spec = 'tonexty'




    if save:
        name = str(data.times[2])
        pl.offline.plot(fig, filename = '.\\outs\\eval_t' + name + '.html', auto_open = False)
    else:
        pl.offline.plot(fig)




def hamlagroPlot(data, save = True):

    #Hamlagrø plot
    time = np.linspace(0,data.T-1,data.T, dtype = int)
    names = ['Volume', 'Production', 'Bypass', 'Penalty']

    #A figure containing 4 reservoir plots, each with multiple schedule traces
    fig = pl.tools.make_subplots(
        rows = 4,
        cols = 1,
        subplot_titles = names,
        print_grid = False
    )

    #Setting layout on the figure (and supressing return output)
    myLayout = fig.layout.update(
        title = 'Hamlagrøvatn schedule',
        width = 800,
        height = 1130,
        legend =  dict(tracegroupgap = 196),
    )

    V_traces = detailPlot(data.V[:,:,1], time, palette = 'blue', legGroup = 'volume')

    for trace in V_traces:
        fig.append_trace(trace, 1, 1)

    Q_traces = detailPlot(data.Q[:,:,1], time, palette = 'blue', legGroup = 'production')

    for trace in Q_traces:
        fig.append_trace(trace, 2, 1)

    B_traces = detailPlot(data.B[:,:,1], time, palette = 'blue', legGroup = 'bypass')

    for trace in B_traces:
        fig.append_trace(trace, 3 , 1)

    penalty_traces = doublePlot([data.gamma[:,:,1], data.O[:,:,1]], time, ['Gamma', 'Overflow'],
                                palettes = ['red', 'blue'], legGroup = 'penalty')

    for trace in penalty_traces:
        fig.append_trace(trace, 4, 1)


    # Environmental constraint traces
    t0,t1,t2,t3,t4 = data.times
    Vmin = pyo.value(data.subs[0][0].EC2V)


    #EC 2
    trace = go.Scatter(
        x = [t2,t2,t3,t3],
        y = [0, Vmin, Vmin, 0],
        name = 'Env2',
        showlegend = False,
        hoverinfo = 'none',
        text = ['t_c', '', '', 't_d'],
        textposition = 'bottom center',
        line = dict(
            color = 'red'
        )
    )
    fig.append_trace(trace, 1, 1)

    #EC 1
    maxY = data.params[1]['maxQ']
    trace = go.Scatter(
        x = [t0,t0,t1,t1,t1,t2,t2],
        y = [0, maxY, maxY, 0, maxY, maxY, 0],
        name = 'Env11',
        showlegend = False,
        hoverinfo = 'none',
        text = ['t_a', ''],
        line = dict(
            color = 'red'
        )
    )
    fig.append_trace(trace, 2, 1)

    maxY = data.params[1]['maxQ']
    trace = go.Scatter(
        x = [t3,t3,t4,t4],
        y = [0, maxY, maxY, 0],
        name = 'Env31',
        showlegend = False,
        hoverinfo = 'none',
        text = ['t_a', ''],
        line = dict(
            color = 'red'
        )
    )
    fig.append_trace(trace, 2, 1)

    maxY = data.B[:,:,1].max()
    trace = go.Scatter(
        x = [t0,t0,t1,t1,t1,t2,t2],
        y = [0, maxY, maxY, 0, maxY, maxY, 0],
        name = 'Env11',
        showlegend = False,
        hoverinfo = 'none',
        text = ['t_a', ''],
        line = dict(
            color = 'red'
        )
    )
    fig.append_trace(trace, 3, 1)

    maxY = data.B[:,:,1].max()
    trace = go.Scatter(
        x = [t3,t3,t4,t4],
        y = [0, maxY, maxY, 0],
        name = 'Env11',
        showlegend = False,
        hoverinfo = 'none',
        text = ['t_a', ''],
        line = dict(
            color = 'red'
        )
    )
    fig.append_trace(trace, 3, 1)

    myLayout['yaxis1'].update(title = 'Mm3')
    myLayout['yaxis2'].update(title = 'Mm3/week')
    myLayout['yaxis3'].update(title = 'Mm3/week')
    # myLayout['yaxis4'].update(title = '')
    myLayout['xaxis4'].update(title = 't')


    if save:
        name = str(data.times[2])
        pl.offline.plot(fig, filename = '.\\outs\\haml_t' + name + '.html', auto_open = False)
    else:
        pl.offline.plot(fig)





def detailPlot(serie, time, palette = 'blue', legGroup = None):
    '''Create traces for a detailed uncertain timeseries plot, for shape (S, T)'''
    traces = []

    if palette == 'red':
        colors = ['rgb(255, 102, 102)', 'rgb(255, 0, 0)', 'rgb(153, 0, 0)', 'rgba(255, 102, 102, 0.4)', 'rgba(255, 0, 0, 0.4)']
    elif palette == 'blue':
        colors = ['rgb(102, 178, 255)', 'rgb(0, 128, 255)', 'rgb(0, 76, 153)', 'rgba(102, 178, 255, 0.4)', 'rgba(0, 128, 255, 0.4)']
    elif palette == 'grey':
        colors = ['rgb(192, 192, 192)', 'rgb(128, 128, 128)', 'rgb(64, 64, 64)', 'rgba(192, 192, 192, 0.4)', 'rgba(128, 128, 128, 0.4)']
    else:
        print('Color not supported')

    quant = np.quantile(serie, [0, 0.25, 0.5, 0.75, 1], axis = 0)

    #min and max traces
    fill_spec = 'none'
    for j in [0,-1]:
        if j == 0:
            showLeg2 = True
        else:
            showLeg2 = False

        trace = go.Scatter(
            x = time,
            y = quant[j,:],
            name = 'Min/max',
            showlegend = showLeg2,
            fill = fill_spec,
            fillcolor = colors[3],
            line = dict(
                color = colors[1]
            ),
            legendgroup = legGroup
        )
        traces.append(trace)
        fill_spec = 'tonexty'

    #Quartile band
    fill_spec = 'none'
    for j in [1,-2]:
        if j == 1:
            showLeg2 = True
        else:
            showLeg2 = False

        trace = go.Scatter(
            x = time,
            y = quant[j,:],
            name = 'Quartiles',
            showlegend = showLeg2,
            fill = fill_spec,
            fillcolor = colors[4],
            hoverinfo = 'none',
            line = dict(
                dash = 'dot',
                color = colors[1]
            ),
            legendgroup = legGroup
        )
        traces.append(trace)
        fill_spec = 'tonexty'

    #Mean trace
    trace = go.Scatter(
        x = time,
        y = quant[2,:],
        name = 'Median',
        showlegend = True,
        line = dict(
            dash = 'dash',
            color = colors[2]
        ),
        legendgroup = legGroup
    )
    traces.append(trace)

    return traces



def doublePlot(series, time, names, palettes = ['grey', 'blue'], legGroup = None):
    '''Double plot for two uncertain timeseries with shape (S, T)'''
    traces = []

    if palettes[0] == 'red':
        colors1 = ['rgb(255, 102, 102)', 'rgb(255, 0, 0)', 'rgb(153, 0, 0)', 'rgba(255, 102, 102, 0.5)', 'rgba(255, 0, 0, 0.5)']
    elif palettes[0] == 'blue':
        colors1 = ['rgb(102, 178, 255)', 'rgb(0, 128, 255)', 'rgb(0, 76, 153)', 'rgba(102, 178, 255, 0.5)', 'rgba(0, 128, 255, 0.5)']
    elif palettes[0] == 'grey':
        colors1 = ['rgb(192, 192, 192)', 'rgb(128, 128, 128)', 'rgb(64, 64, 64)', 'rgba(192, 192, 192, 0.5)', 'rgba(128, 128, 128, 0.5)']
    else:
        print('Color not supported')

    if palettes[1] == 'red':
        colors2 = ['rgb(255, 102, 102)', 'rgb(255, 0, 0)', 'rgb(153, 0, 0)', 'rgba(255, 102, 102, 0.5)', 'rgba(255, 0, 0, 0.5)']
    elif palettes[1] == 'blue':
        colors2 = ['rgb(102, 178, 255)', 'rgb(0, 128, 255)', 'rgb(0, 76, 153)', 'rgba(102, 178, 255, 0.5)', 'rgba(0, 128, 255, 0.5)']
    elif palettes[1] == 'grey':
        colors2 = ['rgb(192, 192, 192)', 'rgb(128, 128, 128)', 'rgb(64, 64, 64)', 'rgba(192, 192, 192, 0.5)', 'rgba(128, 128, 128, 0.5)']
    else:
        print('Color not supported')

    first = True
    k = 1 #Numbering for traces and determining whats what
    for serie in series:
        quant = np.quantile(serie, [0, 0.25, 0.5, 0.75, 1], axis = 0)

        if first:
            colors = colors1
            name = names[0]
        else:
            colors = colors2
            name = names[1]

        #Extreme band
        fill_spec = 'none'
        for i in [0, -1]:
            trace = go.Scatter(
                x = time,
                y = quant[i,:],
                legendgroup = legGroup,
                showlegend = False,
                hoverinfo = 'none',
                name = str(k),
                fill = fill_spec,
                line = dict(
                    width = 0.1,
                    color = colors[0]
                )
            )
            k += 1
            traces.append(trace)
            fill_spec = 'tonexty'

        #Quartile band
        fill_spec = 'none'
        for i in [1, -2]:
            trace = go.Scatter(
                x = time,
                y = quant[i,:],
                legendgroup = legGroup,
                showlegend = False,
                hoverinfo = 'none',
                name = str(k),
                fill = fill_spec,
                line = dict(
                    width = 0.1,
                    color = colors[1]
                )
            )
            k += 1
            traces.append(trace)
            fill_spec = 'tonexty'

        #Mean line
        trace = go.Scatter(
            x = time,
            y = quant[2,:],
            legendgroup = legGroup,
            showlegend = True,
            name = name,
            line = dict(
                color = colors[2],
                width = 2
            )
        )
        traces.append(trace)
        first = False

    return traces






########################Trash########################

# a = np.full(shape = (10,52,2), fill_value = -1)
# def updateSet(a, s, t, i, newind):
#     if a[s,t,i] == -1:
#         a[s,t,i] = newind
#         return a
#
#     else:
#         a[s,t,i] = [a[s,t,i], newind]
#         return a
#
#
# def showSubs(data):
#     s1 = data.subproblems[0]
#     s30 = data.subproblems[29]
#     s52 = data.subproblems[51]
#     lst = [s1, s30, s52]
#     for sub in lst:
#         print('Subproblem ' + sub.name + ' has parameters:')
#         print('V0       Z0      e')
#         for i in range(I):
#             V0 = pyo.value(sub.V0[i])
#             Z0 = pyo.value(sub.Z0[i])
#             e = pyo.value(sub.e[i])
#             str = 'V0 = {:3.3f}   Z0 = {:3.3f}  e = {:3.5f}'.format(V0, Z0, e)
#             print(str)
#
#
#
